---
author: "Thiago Almeida"
linktitle: "First 100km Bike Ride"
title: "First 100km Bike Ride"
date: 2022-04-19T23:10:39Z
tags: ["bike", "en"]
---

Almost an year ago I did my first 100km bike ride.

Together with my friends, we took a tour around the Stockholm archipelago, crossing
several bridges, taking ferries and climbing some crazy hills.

It wasn't easy, and I had to take a few extra laps around my block to reach the
infamous 100km, as my ride officially ended up being only 98km.

<iframe id=strava100km
  height='360'
  width='100%'
  frameborder='0'
  allowtransparency='true'
  scrolling='no'
  src='https://www.strava.com/activities/5424965213/embed/9396d89a19e7c3cf6229c39e9471519a85f718be'>
</iframe>

Among many other perks of being an IT worker living in Sweden, having a great
work-life balance that allows me to spend a lot of time on myself and my many
hobbies is one of my favorites.

Since my first 100km, I've done many other bike rides that were longer than
that, check out [my strava profile](https://www.strava.com/athletes/2749388) to
see my slow pace on those long rides. Maybe we can ride together someday.
