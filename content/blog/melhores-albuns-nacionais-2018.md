---
author: "Thiago Almeida"
linktitle: "Melhores Álbuns Nacionais 2018"
title: "Melhores Álbuns Nacionais 2018"
date: 2018-12-22T23:15:30-02:00
tags: ["musica", "pt-br", "rapbr"]
---

O fim do ano vai chegando e com ele as listas de _melhores do ano_ também.

Uma das listas que eu sempre fico de olho é a de melhores discos do ano
publicada desde 2006 pela Rolling Stone Brasil.

> [Rolling Stone Brasil: os 50 melhores discos nacionais de 2018](https://rollingstone.uol.com.br/noticia/rolling-stone-brasil-os-50-melhores-discos-nacionais-de-2018/) ![](https://rollingstone.uol.com.br/media/_versions/melhores_de_2018_cortada_widelg.jpg "Rolling Stone Brasil: os 50 melhores discos nacionais de 2018")

Achei que a alteração do tamanho da lista de 25 pra 50 foi muito bem vinda,
afinal de contas como diz a matéria original[^1] "A música brasileira vive grande
fase! Intensa, plural, representativa. Como resumir um ano em somente 25
discos?"

Vou destacar aqui alguns desses discos que me acompanharam na
árdua tarefa de encarar esse puxado ano de 2018.

- 41: Dona de Mim, IZA

{{< spotify "spotify:album:6suxiZXNF0F1NC8nPCJG5C" >}}

- 39: Crise, Rashid

{{< spotify "spotify:album:5gtoxo8VoETM0EhENX6k91" >}}

- 34: Ambulante, Karol Conká

{{< spotify "spotify:album:6uZweI7OXLIS8WwYb0eVCV" >}}

- 32: Comunista Rico, Diomedes Chinaski

{{< spotify "spotify:album:3wBM6ulSbKHhctKCeofgGd" >}}

- 21: Gigantes, BK

{{< spotify "spotify:album:4W2IL1NXpcbsYmGdAjeJGg" >}}

- 9: Para Dias Ruins, Mahmundi

{{< spotify "spotify:album:4EsFZtXhyj9RHiRb2V0eMT" >}}

- 8: Sinto Muito, Duda Beat

{{< spotify "spotify:album:1gU6xYQZFJH4ClJcpS1W4w" >}}

- 7: O Menino Que Queria Ser Deus, Djonga

{{< spotify "spotify:album:4mgTW0NU3fjNBxHdKOJoRr" >}}

- 5: Não Para Não, Pabllo Vittar

{{< spotify "spotify:album:7GRhzFj2BulxZBqqOMBdDe" >}}

- 2: Deus É Mulher, Elza Soares

{{< spotify "spotify:album:6EYA1TltIWqEETjRXJx6TA" >}}

- 1: Bluesman, Baco Exu do Blues

{{< spotify "spotify:album:0QMVSKhzT4u2DEd8qdlz4I" >}}

Agora bora comentar de maneira totalmente parcial e não profissional.

Trazendo a tona artistas de vários dos tantos nichos e estilos da música
brasileira essa é sem sombra de dúvidas uma lista diversa e representativa.

O RAP nacional é um dos estilos musicais em alta no momento, assim como o
funk, o tecnobrega, o sertanejo e outros. Sabendo disso, foi muito bom ver como
os jurados escolhidos pela revista estão antenados com o que é música popular
atualmente.

Dentre os 50 álbuns selecionados, temos muitos trabalhos de artistas que,
segundo o Mano Brown[^2], "estão emergindo do inferno". Isso é tão
gratificante que acaba alimentando a alma com esperança pro que há de vir em 2019.

A lista tá repleta de discos que não saíram dos meus fones nos últimos 12
meses e também cheia de ótimas indicações pra escutar nos próximos dias.

Como um bom ~~reclamão e sempre insatisfeito~~ fã de RAP, queria que o disco do
S.C.A do FBC também tivesse uma posição aqui. Então fica de recomendação pra
quando vocês tiverem um tempo.

{{< spotify "spotify:album:4dRdOCgiNFqsjaNBoFGAIY" >}}

Era isso. Até a próxima.

[^1]: https://rollingstone.uol.com.br/noticia/rolling-stone-brasil-os-50-melhores-discos-nacionais-de-2018/
[^2]: https://gauchazh.clicrbs.com.br/cultura-e-lazer/musica/noticia/2018/02/hoje-a-luta-das-pessoas-e-individual-nao-vejo-mais-luta-de-classes-afirma-mano-brown-cjd4ro6d7064k01kexrlfigt4.html
[^3]: https://theintercept.com/2018/11/16/escute-racionais-pague-melhor-autocritica-imprensa/
