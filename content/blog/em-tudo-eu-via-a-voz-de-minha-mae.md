---
author: "Thiago Almeida"
linktitle: "Em Tudo Eu via a Voz De Minha Mae"
title: "Em Tudo Eu via a Voz De Minha Mae"
date: 2025-02-23T14:20:50-03:00
tags: [""]
draft: true
---

Esses dias eu tava falando sobre quando a minha mãe foi me visitar em
Estocolmo, das coisas que ela estava curiosa pra conhecer, das que ela gostou
e, obviamente, das coisas que não entraram na lista de coisas favoritas dela.

Nesse dia eu quis fazer uma homenagem pra ela, e acabei escrevendo e declamando
um poema inspirado na musica Mae do Emicida

{{< youtube D_-j32_Ryc0 >}}

O tal poema:

---
> Desde que eu saí de casa, em tudo eu via a voz de minha mãe
>
> A mesma suavidade encontrei nos aromas das flores novas que conheci
>
> Reparei a mesma força dela nas correntezas dos rios que cortam os horizontes
> mais  exuberantes que avistei
>
> A mesma calma eu senti nas brisa das manhãs dos dias mais confortáveis que já
> vivi
>
> Percebi a mesma curiosidade dela nos olhares das crianças de tudo quanto foi
> lugar que já pisei
>
> Reconheci em mim a paciência dela  todas as vezes que alguém elogiou a
> maneira que eu explico as coisas
>
> A sua  energia sem fim todas as vezes que eu escolhi sorrir, cantar e dançar
> depois de concluir mais uma tarefa impossível
>
> A coragem dela todas as vezes que eu encarei meus medos e conquistei mais um
> espaço que definitivamente não foi feito pra mim
>
> E definitivamente escutei a sua voz me dizendo  "Filho, respira, vai dar tudo
> certo, a gente tá aqui" todas as vezes que eu pensei em desistir
>
> Hoje eu posso mostrar pra ela tudo o que a gente alcançou, correndo junto,
> mesmo que distante
> Mostrar pra ela que em tudo eu via a sua voz
---

E essa e ela em Copenhague durante um dos passeios que a gente fez:
![Mae em Copenhague](https://photos.thiagoalmeida.xyz/api/assets/0df8be06-8387-4df5-b22b-c1ab46aeb021/thumbnail?size=preview&key=fuKVq3IfflKBW_EgYrZ4AYD4obGMsRMBRrSqIJtHAdH_99hLMzy4ROvPZ7aCMG_ytjU)
