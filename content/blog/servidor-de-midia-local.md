---
author: "Thiago Almeida"
linktitle: "Servidor de Mídia Local"
title: "Servidor de Mídia Local"
date: 2023-10-16T22:09:19+02:00
tags: ["pt-br", "k8s", "midia", "tv"]
---

## Pra que fazer download de conteúdo em 2023?

Já que voltamos pra era dos trocentos canais de TV com suas assinaturas
próprias, falar sobre alternativas de acesso livre a conteúdos se mostra ainda
muito relevante.

Não é segredo pra ninguém que a minha geração cresceu usando
[torrent](https://baixacultura.org/2019/09/18/como-usar-torrent-e-baixar-conteudo-compartilhado-gratis/)
pra assistir as séries e filmes que queria. Programas como
[eMule](https://www.emule-project.com/), [LimeWire](https://limewire.com/) e o
meu favorito da época [Ares Galaxy](https://sourceforge.net/projects/aresgalaxy/),
que me permitia mesmo que com uma internet discada lentíssima, eu pudesse ouvir
música e assistir minhas séries e filmes no conforto do meu monitor CRT de 15
polegadas numa época em que ter TV por assinatura era completamente fora do
alcance da minha família e das famílias do meu bairro.

Mas aí veio a promessa do streaming. Serviços como Netflix, iTunes, Spotify e outros
que ofereceram acesso super conveniente por um preço relativamente baixo a
praticamente "qualquer" conteúdo disponível na internet como uma solução
lucrativa para o acesso não autorizado de conteúdo (_pirataria_).
Esses serviços cumpriram sua palavra e fizeram com que a maioria das pessoas
ignorassem completamente a existência dos outrora tão utilizados _torrents_.

De lá pra cá muita coisa mudou, Netflix deixou de ser a única plataforma grande
de streaming, grandes estúdios como Disney, Warner Bros, Paramount entre outros
lançaram suas próprias plataformas e vem batalhando entre si pelo dinheiro do
consumidor no que tem sido chamado de ["batalha do
streaming"](https://www.infomoney.com.br/colunistas/convidados/8-dados-para-entender-a-batalha-do-streaming-e-sua-grande-vitima).

Então agora que a grande comodidade de encontrar todos os seus filmes favoritos
no mesmo aplicativo não existe mais e que a soma das assinaturas necessárias pra
se manter em dia com as produções mais relevantes/interessantes do momento já
não é mais irrisória, configurar um servidor local que automaticamente faz
download do episódio que acabou de ser lançado já com legenda e tudo, parece bem
vantajoso.

---

## Sumário

* [O que vamos montar?](#o-que-vamos-montar)
* [O meu servidor exagerado](#o-meu-servidor-exagerado)
* [Que programas eu uso?](#que-programas-eu-uso)
  * [Sonarr (séries)](#sonarr-séries)
  * [Radarr (filmes)](#radarr-filmes)
  * [Jackett (proxy torrent)](#jackett-proxy-torrent)
  * [Transmission (cliente torrent)](#transmission-cliente-torrent)
  * [NordVPN](#nordvpn)
  * [Jellyfin](#jellyfin)
* [Conclusão](#conclusão)
  * [Referências](#referências)


## O que vamos montar?

No nosso caso, um servidor de mídia pode ser definido como um computador que
monitora, baixa, armazena, organiza e exibe os filmes e séries que a gente quer
assistir assim que eles estiverem disponíveis em qualidade Full HD (nada de
filmagem de tela de cinema).

## O meu servidor exagerado

Assim como todo bom programador empolgado, resolvi que ia rodar um cluster
[Kubernetes](https://kubernetes.io/)(k8s) usando [k3s](https://k3s.io/).
Precisava? Não! Mas quis fazer mesmo assim.

![Resolvendo problemas imaginários de escala](/img/solving-imaginary-scaling-issues.jpg)

Hoje em dia eu rodo um cluster k8s com 3 nós e tô usando como nome os conjuntos
habitacionais (COHAB) lá da [Cidade
Nova](https://pt.wikipedia.org/wiki/Cidade_Nova_(Ananindeua)), bairro de onde
eu sou.

Então até agora tenho:

| nome    	| hardware          	| memória ram 	|   	|   	|
|---------	|-------------------	|-------------	|---	|---	|
| cn4.lan 	| raspberry pi 4    	| 4G          	|   	|   	|
| cn5.lan 	| ThinkCentre M710q 	| 16G         	|   	|   	|
| cn6.lan 	| raspberry pi 4    	| 8G          	|   	|   	|

Ficou curioso com a maluquice que eu fiz, dá uma olhada no código do meu
cluster aqui: <mark>https://github.com/thiagoalmeidasa/homelab</mark>

## Que programas eu uso?

Baseado nos serviços daqui [wiki.servarr.com](https://wiki.servarr.com) e daqui
[linuxserver.io](https://docs.linuxserver.io/)

### Sonarr (séries)

![Sonarr](https://sonarr.tv/img/slider/posters.png)

Sonarr é um aplicativo de rastreamento de série. Este programa facilita a sua
vida, sem a necessidade de se preocupar quando um novo episódio vai ao ar. Ele
pode monitorar diversas séries, e assim busca automaticamente novos episódios,
além de transferir todos os arquivos da sua pasta de mídia, classificando-os e
renomeando-os de acordo com sua configuração.

Minha configuração do [Sonarr](https://github.com/thiagoalmeidasa/homelab/blob/main/kubernetes/apps/media/sonarr/app/helmrelease.yaml)

### Radarr (filmes)

![Radarr](https://radarr.video/img/slider/posters.png)

Radarr é um aplicativo de rastreamento de filmes. Este programa facilita sua
vida, sem a necessidade de se preocupar quando o esperado filme estiver no ar.
Ele pode monitorar seus filmes através de listas como o IMDB, e assim buscar
automaticamente seus filmes, além de transferir todos os arquivos da sua pasta
de mídia, classificando-os e renomeando-os de acordo com sua configuração.

Minha configuração do [Radarr](https://github.com/thiagoalmeidasa/homelab/blob/main/kubernetes/apps/media/radarr/app/helmrelease.yaml)

### Jackett (proxy torrent)

Jackett funciona como um servidor proxy: ele traduz consultas de aplicativos
(Sonarr, Radarr) em consultas http específicas de sites rastreadores como ThePirateBay,
YTS, EZTV e 1337x).

Minha configuração do [Jackett](https://github.com/thiagoalmeidasa/homelab/blob/main/kubernetes/apps/media/jackett/app/helmrelease.yaml)

Mas esse eu tô querendo substituir pelo
[Prowlarr](https://wiki.servarr.com/prowlarr).

Motivação: [Prowlarr is the
Jackett alternative you
need](https://unraid-guides.com/2022/04/29/prowlarr-is-the-jackett-alternative-you-need/)

### Transmission (cliente torrent)

![Transmission](https://transmissionbt.com/assets/images/homepage/carousel_homepage/screenshot_apple_light.png)

Baseado nessa solução [aqui](https://haugene.github.io/docker-transmission-openvpn)

### NordVPN

Outro exagero meu foi conectar todos os serviços usados pra monitorar séries e
filmes numa VPN. Até aí tudo bem, mas eu usei uns conceitos bem específicos de
kubernetes pra isso, tais como injetar um container que comunica com um gateway
vpn toda vez que alguns parâmetros específicos são detectados. Da maneira que eu
configurei, todos os _pods_ de um _namespace_ específico são conectados
automaticamente nesse gateway VPN que por sua vez está conectado com a minha
conta NordVPN.

Baseado nessa documentação: [Roteamento de tráfico de rede usando um pod-gateway](https://web.archive.org/web/20221114161240/https://docs.k8s-at-home.com/guides/pod-gateway/)

Minha configuração: [Pod-gateway](https://github.com/thiagoalmeidasa/homelab/blob/main/kubernetes/apps/vpn-gateway/gateway/app/helmrelease.yaml)

Acho que faço um artigo só sobre esse papo de gateway vpn mais pra frente.

### Jellyfin

![Jellyfin](https://jellyfin.org/assets/images/10.8-home-4a73a92bf90d1eeffa5081201ca9c7bb.png)

O seu próprio site de reprodução de mídia, ou seja, sua Netflix pessoal. É isso
que vamos usar para reproduzir os conteúdos na sua TV e outros dispositivos
como tablets e smartphones.

Você instala e configura um servidor Jellyfin num computador com acesso aos seus
arquivos de mídia e depois um aplicativo cliente nos dispositivos que você usar
para assistir esses conteúdos, ou também pode ser acessado pelo navegador.

Minha configuração do [Jellyfin](https://github.com/thiagoalmeidasa/homelab/blob/main/kubernetes/apps/media/jellyfin/app/helmrelease.yaml)

Aplicativos:

* [Android](https://play.google.com/store/apps/details?id=org.jellyfin.mobile)
* [iOS](https://apps.apple.com/us/app/jellyfin-mobile/id1480192618)
* [Android TV](https://play.google.com/store/apps/details?id=org.jellyfin.androidtv)

## Conclusão

Usei [Kodi](https://kodi.tv/) como servidor multimídia por vários anos,
mas ainda precisava colocar os arquivos lá manualmente, o que era bem pouco
prático. Foi aí que descobri [Sonarr](#sonarr-séries) e
[Radarr](#radarr-filmes), que elevaram bastante o nível de conforto e
simplicidade pra gerenciar as coisas que assisto/quero assistir.

A migração pro [Jellyfin](#jellyfin) aconteceu quando o aplicativo do Kodi parou
de funcionar e eu tive que refazer tudo do zero. Como o Jellyfin eu consigo
configurar dentro do cluster e usar a mesma estratégia de backup que já
tá rodando lá tem anos, isso facilitou muito a minha vida. Fiquei bem feliz com
o resultado e com o fato de que que agora eu posso simplesmente abrir uma aba no
navegador de qualquer dispositivo que eu tenha e dar play no filme ou série que
acabou de ser adicionado lá, sem aplicativo, sem ter que ir na TV ou qualquer
coisa do tipo. Simples de dar play, é assim que eu definiria.

Claro que a maneira que eu fiz esse servidor atual é um
[exagero](#o-meu-servidor-exagerado), e óbvio que dá pra rodar isso tudo num
computador velho sem Kubernetes e essas outras maluquices que eu coloquei, mas
eu me divirto em ter um mini datacenter dentro de casa.

### Referências

1. <https://marcogomes.com/blog/2019/filmes-e-series-na-sua-tv-com-download-gratis-de-lancamentos-em-2019-ficou-mais-facil-fazer-um-media-center-com-raspberry-pi>
2. <https://baixacultura.org/2019/09/18/como-usar-torrent-e-baixar-conteudo-compartilhado-gratis>
3. <https://baixacultura.org/2019/09/25/streaming-e-a-pirataria-digital-atuam-em-parceria>
4. <https://www.infomoney.com.br/colunistas/convidados/8-dados-para-entender-a-batalha-do-streaming-e-sua-grande-vitima>
5. <https://wiki.servarr.com>
6. <https://github.com/thiagoalmeidasa/homelab>
