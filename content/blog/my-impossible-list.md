---
author: "Thiago Almeida"
linktitle: "My Impossible List"
title: "My Impossible List"
tags: ["lifestyle", "impossible-list", "en"]
date: 2018-12-22T13:18:13Z
---

It's been some time since I've been accumulating personal and professional
goals that I believe will improve the way I understand and relate to my life.
So enjoying that today is my first day of vacation and inspired by
[Willian's list](https://willianpaixao.github.io/impossible-list/)
(which in turn is inspired by this
[College Info Geek’s post](https://collegeinfogeek.com/about/meet-the-author/my-impossible-list/)),
I decided to write all those goals in a [list][my impossible list] so they can
be traceable.

As soon as I complete the objectives on the list it will be updated, so you
can view the list and follow the progress on [this link][my impossible list].

[my impossible list]: https://thiagoalmeidasa.gitlab.io/impossible-list/
