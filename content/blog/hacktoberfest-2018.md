---
author: "Thiago Almeida"
linktitle: "Hacktoberfest 2018"
title: "Hacktoberfest 2018"
date: 2018-12-22T18:32:49Z
tags: ["hacktoberfest", "opensource", "github", "en"]
---

This is year was my 4th participation on [Hacktoberfest](https://hacktoberfest.digitalocean.com/)[^1].

In previous editions, I've tried publishing my own open source projects, to
make them publicly available, but this year was different. For the first time,
all the requests I submitted were for real projects and this was amazing. I
felt very good working with these projects, especially with the [Serenata de Amor](https://serenata.ai/)
(other day I stop to write about this amazing project).

Even after the Hacktoberfest period ended, I kept looking at the github and
looking for projects I could collaborate on. With this I have already
submitted another 3 or 4 pull requests for several projects, including one
that I use daily at work.

Now I'm waiting for my gift shirt to arrive. This one will be the fourth and
the picture can be seen here:

[![Hacktoberfest 2018](https://user-images.githubusercontent.com/121322/45907730-f6a80b00-bdad-11e8-93ef-774392192716.png)](https://hacktoberfest.digitalocean.com/ "Hacktoberfest")

As soon as I receive it, I'll take some pictures and post here.

You can follow more information about Hacktoberfest on the
[Github blog][hacktoberfest 2018] and stay tuned for the next year's edition.

Thank you very much and see you next time.

[^1]: [Hacktoberfest](https://hacktoberfest.digitalocean.com/) is a month-long celebration of open source software run by DigitalOcean in partnership with GitHub and Twilio.

[hacktoberfest 2018]: https://blog.github.com/2018-09-24-hacktoberfest-is-back-and-celebrating-its-fifth-year/
