---
author: "Thiago Almeida"
linktitle: "Impossible List"
title: "Impossible List"
date: 2018-12-22T09:32:49Z
tags: ["lifestyle"]
---

# Impossible List

Inspired by [Willian's post](https://willianpaixao.github.io/impossible-list/),
I will try to keep up with my performance against some personal challenges.

## Life

- [ ] Publish or collaborate regularly with a podcast.
- [ ] Read more than 5 non-work related books in a year.
- [ ] Write my own conduct code.
- [ ] Publish at least 20 articles on my blog in a year.
- [ ] Visit at least 5 countries with my parents.
- [ ] Help my father to sell 1k items in his e-commerce store.

## Fitness

- [ ] Run 360km in a year.
- [ ] Do 100 sit-ups in a single set.
- [ ] Do 100 push-ups in a single set.
- [ ] Do 100 leg raises in a single set.
- [x] ~~Do a 30km bike ride.~~[My First 100km Bike Ride]({{<ref "/blog/first-100km-bike-ride">}})
- [ ] Participate in a Wushu Championship.
- [x] ~~Compete in a volleyball tournament.~~

## Travel

### In Brazil

- [x] ~~Visit Salvador.~~ 2020-02
- [ ] Visit [Lençóis Maranhenses](https://en.wikipedia.org/wiki/Len%C3%A7%C3%B3is_Maranhenses_National_Park)
- [x] ~~Visit Rio de Janeiro.~~ 2018-12
- [x] ~~Visit Florianópolis.~~ 2018-08
- [ ] Visit [Pipa](https://en.wikipedia.org/wiki/Pipa_Beach)

### Outside Brazil

- [ ] Visit Cape Verde.
- [ ] Visit Chile.
- [ ] Visit Cuba.
- [x] ~~Visit Czech Republic.~~ 2021-10
- [ ] Visit Dominican Republic.
- [ ] Visit Egypt.
- [x] ~~Visit Germany.~~ 2021-09
- [x] ~~Visit Iceland.~~ 2019-07
- [ ] Visit Italy.
- [ ] Visit Jamaica.
- [ ] Visit Peru.
- [ ] Visit Puerto Rico.
- [ ] Visit Costa Rica.

## Professional

### Community

- [ ] Actively contribute to a tech community.
- [ ] Build an NGO in the neighborhood where I grew up.
- [ ] Organize a big non-tech event in the neighborhood where I grew up.
- [ ] Organize a big tech event in the neighborhood where I grew up.

### Computer languages

- [x] [~~Publish an open source python tool.~~]({{< ref "projects">}})
- [ ] Publish an open source golang tool.
