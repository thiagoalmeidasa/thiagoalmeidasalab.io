# My personal website

## First steps

1. [Install hugo](https://gohugo.io/getting-started/installing/#linux)

2. Clone this repo with the submodules by running this command:

```bash
git clone --recursive https://gitlab.com/thiagoalmeidasa/thiagoalmeidasa.gitlab.io.git
```

3. Enter in code directory:

```bash
cd thiagoalmeidasa.gitlab.io
```

4. Execute hugo server with `--buildDrafts` option to include content marked
   as draft in the pre visualization:

```bash
hugo server --bind=0.0.0.0 -D
```
